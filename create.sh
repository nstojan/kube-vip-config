docker run --network host --rm ghcr.io/kube-vip/kube-vip:v0.3.8  manifest daemonset \
    --interface ens192 \
    --vip 192.168.50.200 \
    --controlplane \
    --services \
    --inCluster \
    --taint \
    --arp